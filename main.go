package main

import (
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"regexp"

	"bitbucket.org/antizealot1337/patterndl/utils"
)

const (
	version = "1.0"
)

func main() {
	// Values for the command line arguments
	var showVer bool
	var pattern string
	outputdir := "."

	// Create the command line arguments
	flag.BoolVar(&showVer, "version", false, "Print the version and exit")
	flag.StringVar(&pattern, "pattern", "", "The extension or pattern to match")
	flag.StringVar(&outputdir, "dir", outputdir, "The output directory")

	// Parse the command line arguments
	flag.Parse()

	// Check if the version should be shown
	if showVer {
		fmt.Println(version)
		return
	} //if

	// Check for at least one web site
	if flag.NArg() == 0 {
		// Println the error
		fmt.Fprintf(os.Stderr, "Expected address to at least one web site.\n"+
			"Run \"%s -usage\" for more information\n", os.Args[0])

		// Exit with an error status code
		os.Exit(-1)
	} //if

	// Prepare the pattern
	re, err := regexp.Compile(pattern)

	// Check for an error
	if err != nil {
		// Print the error
		fmt.Fprintf(os.Stderr, "Pattern was invalid. %s\n", err)

		// Exit with an error status code
		os.Exit(-2)
	} //if

	// NOTE: All errors from this point on will be logged to stderr but will not
	// terminate the program

	// Create client to be used for all http connections
	client := http.Client{}

	// Get the links from the sites
	links := getLinks(&client, re, flag.Args())

	// Now download the links
	downloadAll(&client, outputdir, links...)
} //func

func getLinks(client *http.Client, re *regexp.Regexp, sites []string) (links []string) {
	// Create the link channel
	linkCh := make(chan string, 0)

	// Create the error/done channel
	edCh := make(chan error, 0)

	// Loop through the sites
	for _, site := range sites {
		// Make the request
		resp, err := client.Get(site)

		// Check for an error
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error retrieving %s. %s\n", site, err)
			continue
		} //if

		// Make sure the body is closed after this loop
		defer resp.Body.Close()

		// Read the site
		go utils.FindLinks(resp.Body, re, site, linkCh, edCh)

		for running := len(sites); running > 0; {
			select {
			case link := <-linkCh:
				// Add the links
				links = append(links, link)
			case err := <-edCh:
				// Check for an error
				if err != nil {
					fmt.Fprintf(os.Stderr, "Error parsing %s. %s\n", site, err)
				} //if

				// Decrease the running count
				running--
			} //select
		} //for
	} //for

	return
} //func

func downloadAll(client *http.Client, outdir string, links ...string) {
	// Loop through the links and download them
	for _, link := range links {
		// Make the request
		resp, err := client.Get(link)

		// Check for an error
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error downloading %s. %s\n", link, err)
			continue
		} //if

		// Make sure the response body is closed at the end of this loop
		defer resp.Body.Close()

		// Create the output file
		out, err := os.Create(utils.OutputPath(link, outdir))

		// Check for an error
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error creating %s. %s\n", link, err)
			continue
		} //if

		// Make sure the file is closed at the end of the this loop
		defer out.Close()

		// Copy from the body to the file
		_, err = io.Copy(out, resp.Body)

		// Check for an error
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error downloading %s. %s\n", link, err)
		} else {
			fmt.Printf("Downloaded %s.\n", out.Name())
		} //if
	} //for
} //func
