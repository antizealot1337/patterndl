package utils

import (
	"bytes"
	"io"
	"net/url"
	"path"
	"regexp"
	"strings"

	"golang.org/x/net/html"
)

// FindLinks will parse a reader for nodes with "src" or "href" attributes and
// check them against the provided regular expression. It will construct a
// complete URL by using the origin and will report the link on the links
// channel. If an error occures it will be reported on the done channel. If the
// reader is exhausted then nil will be sent to the done channel.
func FindLinks(r io.Reader, re *regexp.Regexp, origin string, links chan string, done chan error) {
	// Create a tokenizer
	tokenizer := html.NewTokenizer(r)

	// Loop
	for {
		// Get the next token type
		tokt := tokenizer.Next()

		switch tokt {
		case html.StartTagToken, html.SelfClosingTagToken:
			// Loop through the attributes looking for either href or src
			for k, v, more := tokenizer.TagAttr(); true; k, v, more = tokenizer.TagAttr() {
				// Check the attribute
				switch attr := string(k); attr {
				case "href", "src":
					if re.Match(v) {
						// Send the value to
						links <- prepLink(origin, v)
					} //if
				} //switch

				if !more {
					break
				} //if
			} //for
		case html.ErrorToken:
			// Get the error
			if err := tokenizer.Err(); err == io.EOF {
				done <- nil
			} else {
				done <- err
			} //if

			// Done parsing
			return
		} //switch
	} //for
} //func

func prepLink(origin string, link []byte) string {
	if absolute(link) {
		return string(link)
	} //if

	// Check if the link begins with a "/"
	if link[0] == '/' {
		return origin + string(link)
	} //if

	return origin + "/" + string(link)
} //func

func absolute(link []byte) bool {
	return bytes.HasPrefix(link, []byte("http://")) ||
		bytes.HasPrefix(link, []byte("https://"))
} //func

// OutputPath takes a link and an output directory and returns the path to output
// to that includes the file from the link and is in the outdir.
func OutputPath(link string, outdir string) string {
	// Unescape the name
	var err error
	link, err = url.QueryUnescape(link)

	if err != nil {
		panic(err)
	} //if

	// Attempt to find just the file name
	if idx := strings.LastIndexByte(link, '/'); idx != -1 {
		return path.Join(outdir, link[idx:])
	} //if

	return path.Join(outdir, link)
} //func
