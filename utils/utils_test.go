package utils

import (
	"bytes"
	"regexp"
	"testing"
)

func TestFindLinks(t *testing.T) {
	// Create a regular expression
	re := regexp.MustCompile("dl$")

	// A bad source
	src := bytes.NewBufferString(
		`<!DOCTTYPE html><html lang="en">
    <head></head>
    <body>
    <a href="test1.dl"></a>
    <a src="http://test.org/test2.dl"></a>
    <a href="bad"></a>
    </body>
    </html>`)

	// Create a channel for the links
	links := make(chan string, 0)

	// Create a channel for done
	done := make(chan error, 0)

	// Find the links
	go FindLinks(src, re, "http://example.org", links, done)

	// The links that were read from the source
	var lnks []string

done:
	for {
		select {
		case link := <-links:
			lnks = append(lnks, link)
		case <-done:
			break done
		} //select
	} //for

	// Check the number of links
	if expected, actual := 2, len(lnks); actual != expected {
		if actual < expected {
			t.Fatalf("Expected %d links but there were %d", expected, actual)
		} else {
			t.Errorf("Expected %d links but there were %d", expected, actual)
		} //if
	} //if

	// Check the first
	if expected, actual := "http://example.org/test1.dl", lnks[0]; actual != expected {
		t.Errorf(`Expected link to be "%s" but was "%s"`, expected, actual)
	} //if

	// Check the second
	if expected, actual := "http://test.org/test2.dl", lnks[1]; actual != expected {
		t.Errorf(`Expected link to be "%s" but was "%s"`, expected, actual)
	} //if

	// Close the channels
	close(links)
	close(done)
} //func

func TestPrepLink(t *testing.T) {
	// The origin
	origin := "http://example.org"

	// Prepare an absolute url link
	if expected, actual := "http://test.org/something.js", prepLink(origin,
		[]byte("http://test.org/something.js")); actual != expected {
		t.Errorf(`Expected the link to be "%s" but was "%s"`, expected, actual)
	} //if

	// Prepare a relative url link
	if expected, actual := "http://example.org/something.js", prepLink(origin,
		[]byte("something.js")); actual != expected {
		t.Errorf(`Expected the link to be "%s" but was "%s"`, expected, actual)
	} //if

	// Prepare a root relative url link
	if expected, actual := "http://example.org/something.js", prepLink(origin,
		[]byte("/something.js")); actual != expected {
		t.Errorf(`Expected the link to be "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestAbsolute(t *testing.T) {
	// Check if the url is absolute
	if url := "test/something.js"; absolute([]byte(url)) {
		t.Errorf(`Did not expect "%s" to be an absoule url`, url)
	} //if

	// Check if the url is absolute
	if url := "https://test.org/something.js"; !absolute([]byte(url)) {
		t.Errorf(`Expected "%s" to be an absoule url`, url)
	} //if
} //func

func TestOutputPath(t *testing.T) {
	// The data for the output path
	outdir, url := "testdata", "http://example.org/js/test.js"

	// Check the output directory
	if expected, actual := "testdata/test.js", OutputPath(url, outdir); actual != expected {
		t.Errorf(`Expected path to be "%s" but was "%s"`, expected, actual)
	} //if
} //func
